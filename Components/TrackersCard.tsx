import { FlatList, StyleSheet, Text, View,Dimensions, Image } from 'react-native';
import trackerItems from './TrackersData';

const {width}=Dimensions.get("screen");
const cardWidth=width/2-20;


type tracker = {
  id ?: string,
  image: string,
  name ?: string,
  title: string,
  description: string,  
}

type ItemProps = {trackItem: tracker}

const Card = ({trackItem}:ItemProps) => {
  return(
    <View style={styles.card}>
    <View style={{alignItems: 'center'}}>
      <Image source={{uri:trackItem.image}} style={{height: 70,width: 70,margin:10}}></Image>
      <Text style={{margin:10,fontSize:20,fontWeight:'bold',fontFamily:''}}>{trackItem.title}</Text>
      <Text style={{margin:10,fontFamily: '',fontSize:15,color: '#72929c'}}>{trackItem.description}</Text>
    </View>
  </View>
  );
}

const TrackerCard= () => {
    return (
        <View>
         <FlatList
         showsVerticalScrollIndicator={false}
         numColumns={2}
         data={trackerItems}
         renderItem={({item}) => <Card trackItem={item} />}
         />
        </View>
      );
}

const styles = StyleSheet.create({
      card: {
      height: 210,
      width: cardWidth,
      marginHorizontal: 10,
      marginBottom: 20,
      marginTop: 50,
      borderRadius:15,
      elevation:13,
      backgroundColor: '#F5FFFA',
      alignItems:'center',
    }
  });

  export default TrackerCard;
