 const trackerItems = [
    {
      id: '1',
      image: 'https://gitlab.com/kavita.j.rana/fitbuddy/uploads/9257cf7ef3bbeca5b7f74d021a94391f/food.png',
      name: 'Calorie Tracker',
      title: 'Calorie Tracker',
      description: 'Track calories consumed in a day',
    },
    {
      id: '2',
      image: 'https://gitlab.com/kavita.j.rana/fitbuddy/uploads/8ca9e5b05fd4328926ce232f2cf7dc98/step.png',
      name: 'Weight Tracker',
      title: 'Weight Tracker',
      description: 'Track your current and target weight',
    },
    {
      id: '3',
      image: 'https://gitlab.com/kavita.j.rana/fitbuddy/uploads/b9d591a33968d25780e2507b7d2b6424/weight.png',
      name: 'Steps Tracker',
      title: 'Steps Tracker',
      description: 'Track total steps count in a day',
    },
    {
      id: '4',
      image: 'https://gitlab.com/kavita.j.rana/fitbuddy/uploads/43f747b99f15bbd2712080f32edb16a7/water.png',
      name: 'Water Tracker',
      title: 'Water Tracker',
      description: 'Track water intake in a day',
    },
  ]

  export default trackerItems;