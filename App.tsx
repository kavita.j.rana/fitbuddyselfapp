import {StyleSheet, Text, View, StatusBar} from 'react-native';
import TrackerCard from './Components/TrackersCard';


export default function App() {
  return (
    <>
    <View style={styles.container}>
     <Text style={styles.title}>Trackers</Text>
     <TrackerCard/>
    </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    display:'flex',
    marginTop: StatusBar.currentHeight || 0,
    justifyContent:'space-evenly',
    alignItems:'flex-start',
    backgroundColor: '#b59f9e',
  },
  title:{
    alignItems:'flex-start',
    marginTop:30,
    marginLeft:10,
    fontSize:20,
    fontWeight:'bold',
    fontFamily:''
  }
});
